#include "AbsCalculator.hpp"
#include "Types.hpp"
#include "AnalyzerParameters.hpp"

#include <vector>
#include <cmath>

#include <gtest/gtest.h>

TEST(AbsCalculatorTest, CalculateAbs)
{
    const int32_t fftSize           = 32;
    const int32_t numOfFfts         = 2;
    const int32_t totalSamplesCount = fftSize * numOfFfts;
    std::vector<c64>    fftOutput;
    std::vector<double> postDetInput;
    c64 inputValue = c64(1.f, 1.f);
    fftOutput.assign(totalSamplesCount, inputValue);
    postDetInput.assign(totalSamplesCount, 0.f);

    Buffers buffers;
    buffers.postDetectionInput = &postDetInput[0];
    buffers.fftOutput = &fftOutput[0];

    CommonParams commonParams;
    commonParams.fftSize   = fftSize;
    commonParams.numOfFfts = numOfFfts;


    AbsCalculator calculator(commonParams, buffers);
    calculator.Execute();

    const double refValue = std::abs(inputValue);
    for(int32_t idx = 0; idx < totalSamplesCount; ++idx)
    {
        EXPECT_DOUBLE_EQ(refValue, postDetInput[idx]) << "idx = " << idx;
    }
}
