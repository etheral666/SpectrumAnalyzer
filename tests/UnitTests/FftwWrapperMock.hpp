#ifndef FFTW_MOCK_HPP
#define FFTW_MOCK_HPP

#include "IFftw.hpp"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

class FftwMock : public IFftw
{
public:
    MOCK_METHOD1(FftwMalloc, void*(std::size_t n));
    MOCK_METHOD1(FftwFree, void (void* p));
    MOCK_METHOD1(FftwDestroyPlan, void(fftw_plan plan));
    MOCK_METHOD5(FftwPlanDft1d, fftw_plan(int           n,
                                          fftw_complex* input,
                                          fftw_complex* output,
                                          int           sign,
                                          unsigned      flags));
    MOCK_METHOD7(FftwCreatePlanSpecific, fftw_plan(int            n,
                                                   fftw_direction dir,
                                                   int            flags,
                                                   fftw_complex*  in,
                                                   int            istride,
                                                   fftw_complex*  out,
                                                   int            ostride));
    MOCK_METHOD8(FftwExecute, void(fftw_plan     plan,
                                   int           howmany,
                                   fftw_complex* in,
                                   int           istride,
                                   int           idist,
                                   fftw_complex* out,
                                   int           ostride,
                                   int           odist));
};

#endif // FFTW_MOCK_HPP
