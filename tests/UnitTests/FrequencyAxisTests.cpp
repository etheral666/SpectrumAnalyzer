#include "FrequencyAxis.hpp"
#include "IAxis.hpp"
#include "Defaults.hpp"
#include "Constants.hpp"
#include "AnalyzerParameters.hpp"

#include <cstring>

#include <gtest/gtest.h>

using namespace testing;

class FrequencyAxisTests : public Test
{
public:
    FrequencyAxisTests()
        : m_axis(m_commonParams, m_axesParams, m_buffers)
    {
        std::memset(m_freqencyBuffer, 0, Defaults::pointsOnPlot * sizeof(double));
        m_buffers.xValues = m_freqencyBuffer;
    }

protected:
    double m_freqencyBuffer[Defaults::pointsOnPlot];

    CommonParams  m_commonParams;
    AxesParams    m_axesParams;
    Buffers       m_buffers;
    FrequencyAxis m_axis;
};

TEST_F(FrequencyAxisTests, UpdateValues)
{
    m_commonParams.startFrequency = Defaults::startFrequency;
    m_commonParams.samplingRate   = Defaults::samplingRate;
    m_commonParams.fftSize        = Const::fftSize2048;

    m_axesParams.pointsToDisplay = Defaults::pointsOnPlot;
    m_axesParams.xAxisDivider    = Const::noScaler;

    m_axis.UpdateValues();

    const double decimationRatio = static_cast<double>(m_commonParams.fftSize) / m_axesParams.pointsToDisplay;
    double refFrequency  = m_commonParams.startFrequency;
    double frequencyStep = m_commonParams.samplingRate / m_commonParams.fftSize * decimationRatio;
    for(int32_t idx = 0; idx < m_axesParams.pointsToDisplay; ++idx)
    {
        EXPECT_DOUBLE_EQ(refFrequency, m_freqencyBuffer[idx]) << "idx=" << idx;
        refFrequency += frequencyStep;
    }

    EXPECT_DOUBLE_EQ(m_axesParams.xMin, m_freqencyBuffer[0]);
    EXPECT_DOUBLE_EQ(m_axesParams.xMax, m_freqencyBuffer[m_axesParams.pointsToDisplay - 1]);
}
