#include "HoldMaxAxis.hpp"
#include "AnalyzerParameters.hpp"
#include "Types.hpp"

#include <vector>

#include <gtest/gtest.h>

TEST(HoldMaxAxisTest, UpdateValues)
{
    CommonParams commonParams;
    commonParams.fftSize = 2048;
    AxesParams   axesParams;
    axesParams.pointsToDisplay = 1024;

    std::vector<double> input;
    input.assign(commonParams.fftSize, 1.f);

    std::vector<double> output;
    output.assign(axesParams.pointsToDisplay, 0.f);

    Buffers buffers;
    buffers.postDetectionOutput = &input[0];
    buffers.yValues             = &output[0];

    HoldMaxAxis axis(commonParams, axesParams, buffers);

    axis.UpdateValues();
    for(int32_t idx = 0; idx < axesParams.pointsToDisplay; ++idx)
    {
        EXPECT_DOUBLE_EQ(1.f, output[idx]);
    }

    input.assign(commonParams.fftSize, 2.f);
    axis.UpdateValues();
    for(int32_t idx = 0; idx < axesParams.pointsToDisplay; ++idx)
    {
        EXPECT_DOUBLE_EQ(2.f, output[idx]);
    }

    input.assign(commonParams.fftSize, 1.f);
    axis.UpdateValues();
    for(int32_t idx = 0; idx < axesParams.pointsToDisplay; ++idx)
    {
        EXPECT_DOUBLE_EQ(2.f, output[idx]);
    }
}
