#include "Fftw.hpp"
#include "Types.hpp"

#include <algorithm>
#include <vector>
#include <iostream>
#include <gtest/gtest.h>

//TEST(FftwTestingGround, MultipleFfts)
//{
//    int32_t fftSize     = 32;
//    int32_t numOfFfts   = 4;
//    int32_t inOutStride = 1;

//    std::vector<c64> input;
//    std::vector<c64> output;
//    input.reserve(numOfFfts * fftSize);
//    output.reserve(numOfFfts * fftSize);


//    Fftw fftw;
//    fftw_plan plan = fftw.FftwCreatePlanSpecific(fftSize,
//                                                 FFTW_FORWARD,
//                                                 FFTW_MEASURE,
//                                                 reinterpret_cast<fftw_complex*>(&input[0]),
//                                                 inOutStride,
//                                                 reinterpret_cast<fftw_complex*>(&output[0]),
//                                                 inOutStride);
//    std::fill_n(&input[0], numOfFfts * fftSize, c64(1.f, 0.f));
//    std::fill_n(&output[0], numOfFfts * fftSize, c64(10.f, 10.f));

//    fftw.FftwExecute(plan,
//                     numOfFfts,
//                     reinterpret_cast<fftw_complex*>(&input[0]),
//                     inOutStride,
//                     fftSize,
//                     reinterpret_cast<fftw_complex*>(&output[0]),
//                     inOutStride,
//                     fftSize);
//    std::cout << "finished" << std::endl;
//    for(int32_t idx = 0; idx < fftSize * numOfFfts; ++idx)
//    {
//        std::cout << "output[" << idx << "]=(" << output[idx].real() << "," << output[idx].imag() << ")" << std::endl;
//    }
//    std::cout << "out:"<<std::abs(output[0]) << " ref:" << std::abs(c64(1.f, 0.f)) * fftSize << std::endl;
//}
