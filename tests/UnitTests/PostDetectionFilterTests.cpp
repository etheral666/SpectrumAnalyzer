#include "PostDetectionFilter.hpp"
#include "Types.hpp"
#include "AnalyzerParameters.hpp"

#include <cstring>
#include <algorithm>
#include <vector>

#include <gtest/gtest.h>

using namespace testing;

class PostDetectionFilterTest : public Test
{
public:
    PostDetectionFilterTest()
        : m_filter(m_commonParams, m_buffers)
    {}

    void PrepareParams(const int32_t fftSize, const int32_t numOfFfts)
    {
        m_postDetInput.reserve(numOfFfts * fftSize);
        m_postDetOutput.reserve(fftSize);

        m_buffers.postDetectionInput  = &m_postDetInput[0];
        m_buffers.postDetectionOutput = &m_postDetOutput[0];

        m_commonParams.fftSize             = fftSize;
        m_commonParams.numOfFfts           = numOfFfts;
    }

protected:
    CommonParams m_commonParams;
    Buffers      m_buffers;
    std::vector<double> m_postDetInput;
    std::vector<double> m_postDetOutput;

    PostDetectionFilter m_filter;
};

TEST_F(PostDetectionFilterTest, SingleBufferAveraging)
{
    const int32_t numOfFfts = 1;
    const int32_t fftSize   = 16;
    PrepareParams(fftSize, numOfFfts);

    const double refValue = 1.f;
    std::fill_n(&m_postDetInput[0], fftSize, refValue);
    std::fill_n(&m_postDetOutput[0], fftSize, 0.f);

    m_filter.Execute();

    for(int32_t idx = 0; idx < fftSize; ++idx)
    {
        EXPECT_DOUBLE_EQ(refValue, m_postDetOutput[idx]);
    }
}

TEST_F(PostDetectionFilterTest, MultipleBuffersAveraging)
{
    const int32_t numOfFfts = 4;
    const int32_t fftSize   = 16;
    PrepareParams(fftSize, numOfFfts);

    double inputValue = 1.f;
    for(int32_t fftResult = 0; fftResult < numOfFfts; ++fftResult)
    {
        std::fill_n(&m_postDetInput[fftSize * fftResult], fftSize, inputValue);
        inputValue += 2.f;
    }
    std::fill_n(&m_postDetOutput[0], fftSize, 0.f);

    m_filter.Execute();

    double averagingResult = (1.f + 3.f + 5.f + 7.f) / 4.f;
    for(int32_t idx = 0; idx < fftSize; ++idx)
    {
        EXPECT_DOUBLE_EQ(averagingResult, m_postDetOutput[idx]) << "idx=" << idx;
    }
}
