#include "DecibelScaler.hpp"
#include "Constants.hpp"
#include "AnalyzerParameters.hpp"

#include <vector>

#include <gtest/gtest.h>

TEST(DecibelScalerTests, ApplyLogScale)
{
    const int32_t fftSize = 16;
    std::vector<double> postDetectionOut;
    postDetectionOut.assign(fftSize, 100.f);

    Buffers buffers;
    buffers.postDetectionOutput = &postDetectionOut[0];

    CommonParams commonParams;
    commonParams.fftSize             = fftSize;

    DecibelScaler scaler(commonParams, buffers);
    scaler.Execute();

    for(int32_t idx = 0; idx < fftSize; ++idx)
    {
        EXPECT_DOUBLE_EQ(20.f, buffers.postDetectionOutput[idx]) << "idx=" << idx;
    }
}
