#include "FftwWrapper.hpp"
#include "FftwWrapperMock.hpp"
#include "Constants.hpp"
#include "AnalyzerParameters.hpp"

#include <memory>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

using namespace testing;

class FftwWrapperTest : public Test
{
public:
    FftwWrapperTest()
        : m_inOutStride(1)
    {
        m_buffers.fftInput  = NULL;
        m_buffers.fftOutput = NULL;

        m_commonParams.samplesCount = Const::hackRfTransferSamplesCount;
        m_commonParams.fftSize      = Const::fftSize16384;

        m_fftParams.fftwEngine = &m_fftwMock;
        m_fftParams.fftSign    = FFTW_FORWARD;
        m_fftParams.fftFlags   = FFTW_MEASURE;

        const int32_t mallocSize = sizeof(c64) * m_commonParams.samplesCount;
        EXPECT_CALL(m_fftwMock, FftwMalloc(mallocSize)).Times(2);
        EXPECT_CALL(m_fftwMock, FftwCreatePlanSpecific(m_commonParams.fftSize,
                                                       m_fftParams.fftSign,
                                                       m_fftParams.fftFlags,
                                                       _,
                                                       m_inOutStride,
                                                       _,
                                                       m_inOutStride));
        m_fftWrapper.reset(new FftwWrapper(m_commonParams, m_fftParams, m_buffers));
    }

    ~FftwWrapperTest()
    {
        ExpectCleanup();
    }

    void ExpectCleanup()
    {
        InSequence inSeq;

        EXPECT_CALL(m_fftwMock, FftwDestroyPlan(m_fftParams.fftwSpecificPlan));
        EXPECT_CALL(m_fftwMock, FftwFree(m_buffers.fftInput)).Times(1);
        EXPECT_CALL(m_fftwMock, FftwFree(m_buffers.fftOutput)).Times(1);
    }

    void ExpectPlanChange()
    {
        ExpectCleanup();
        const int32_t mallocSize = sizeof(c64) * m_commonParams.samplesCount;
        EXPECT_CALL(m_fftwMock, FftwMalloc(mallocSize)).Times(2);
        EXPECT_CALL(m_fftwMock, FftwCreatePlanSpecific(m_commonParams.fftSize,
                                                       m_fftParams.fftSign,
                                                       m_fftParams.fftFlags,
                                                       _,
                                                       m_inOutStride,
                                                       _,
                                                       m_inOutStride));
    }

protected:
    const int32_t m_inOutStride;

    StrictMock<FftwMock>         m_fftwMock;
    std::unique_ptr<FftwWrapper> m_fftWrapper;

    CommonParams m_commonParams;
    FftParams    m_fftParams;
    Buffers      m_buffers;
};

TEST_F(FftwWrapperTest, ComputingFFT)
{
    EXPECT_CALL(m_fftwMock, FftwExecute(m_fftParams.fftwSpecificPlan,
                                        m_commonParams.numOfFfts,
                                        reinterpret_cast<fftw_complex*>(m_buffers.fftInput),
                                        m_inOutStride,
                                        m_commonParams.fftSize,
                                        reinterpret_cast<fftw_complex*>(m_buffers.fftOutput),
                                        m_inOutStride,
                                        m_commonParams.fftSize));
    m_fftWrapper->ComputeFFT();
}

TEST_F(FftwWrapperTest, ChangingFftwPlan)
{
    ExpectPlanChange();
    m_fftWrapper->UpdateFftwParameters();
}



