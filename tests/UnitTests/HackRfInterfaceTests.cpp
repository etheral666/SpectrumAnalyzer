#include "HackRfInterface.hpp"
#include "Constants.hpp"
#include "Globals.hpp"
#include "Types.hpp"

#include <gtest/gtest.h>

#include <cstring>

using namespace testing;

class HackRfInterfaceTest : public Test
{
public:
    HackRfInterfaceTest()
        : m_bufferSize(Const::hackRfTransferBufferSize),
          m_hackRf(NULL)
    {
        std::memset(&Global::lastTransferInfo, 0, sizeof(hackrf_transfer));
        m_hackRf = HackRfInterface::GetInstance();
        CheckInitialization();
    }

    void CheckInitialization()
    {
        HackRfInterface* null = NULL;
        ASSERT_NE(null, m_hackRf);
        ASSERT_TRUE(m_hackRf->IsReady()) << m_hackRf->GetLastErrorDescription();
    }

    void CheckTransferInfo()
    {
        EXPECT_EQ(m_bufferSize, Global::lastTransferInfo.buffer_length);
        EXPECT_EQ(m_bufferSize, Global::lastTransferInfo.valid_length);
    }

protected:
    const int32_t m_bufferSize;

    HackRfInterface* m_hackRf;
};

TEST_F(HackRfInterfaceTest, DISABLED_StartStreaming)
{
    m_hackRf->StartReceiving();
    ASSERT_TRUE(m_hackRf->IsReady()) << m_hackRf->GetLastErrorDescription();
    m_hackRf->WaitForNextFrame();
    CheckTransferInfo();
}

