#ifndef DEFAULTS_HPP
#define DEFAULTS_HPP

#include "Types.hpp"

struct Defaults
{
    static const int64_t startFrequency = 85000000;         // 85 MHz
    static const int32_t samplingRate   = 10000000;         // 10 MHz
    static const int32_t bandwidth      = samplingRate / 2; //  5 MHz

    static const int32_t pointsOnPlot = 1024;

    static const int32_t hackRfIfGain  = 40; // intermediate frequency dB gain
    static const int32_t hackRfVgaGain = 0; // baseband dB gain
};

#endif // DEFAULTS_HPP

