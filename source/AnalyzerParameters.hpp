#ifndef ANALYZER_PARAMETERS_HPP
#define ANALYZER_PARAMETERS_HPP

#include "Types.hpp"
#include "IFftw.hpp"
#include "IDspChainLink.hpp"
#include "IAxis.hpp"

#include <vector>

struct Buffers
{
    c64* fftInput;
    c64* fftOutput;

    double* postDetectionInput;
    double* postDetectionOutput;

    double* xValues;
    double* yValues;
};

struct CommonParams
{
    double startFrequency;
    double samplingRate;
    double bandwidth;

    int32_t fftSize;
    int32_t numOfFfts;

    int32_t samplesCount;
};

struct HackRfParams
{
    int32_t vgaGain;
};

struct FftParams
{
    IFftw* fftwEngine;

    fftw_direction fftSign;
    uint32_t       fftFlags;

    fftw_plan fftwSpecificPlan;
};

struct DspParams
{
    std::vector<IDspChainLink*> dspChain;

    IDspChainLink* absCalculator;
    IDspChainLink* postDetector;
    IDspChainLink* dbFsScaler;
    IDspChainLink* dbScaler;
};

struct AxesParams
{
    IAxis* yAxisCalculator;
    IAxis* decimationAxis;
    IAxis* holdMaxAxis;

    int32_t pointsToDisplay;
    int32_t xAxisDivider;

    double xMin;
    double xMax;
    double yMin;
    double yMax;

    char* xAxisLabel;
};

struct AnalyzerParameters
{
    Buffers      buffers;
    CommonParams common;
    HackRfParams hackRfParams;
    FftParams    fft;
    DspParams    dsp;
    AxesParams   axes;
};

#endif // ANALYZER_PARAMETERS_HPP

