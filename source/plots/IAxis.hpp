#ifndef IAXIS_HPP
#define IAXIS_HPP

#include "Types.hpp"

class IAxis
{
public:
    virtual void UpdateValues() = 0;
};

#endif // IAXIS_HPP

