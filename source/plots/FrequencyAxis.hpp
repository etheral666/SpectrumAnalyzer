#ifndef FREQUENCY_AXIS_HPP
#define FREQUENCY_AXIS_HPP

#include "IAxis.hpp"

struct CommonParams;
struct AxesParams;
struct Buffers;

class FrequencyAxis : public IAxis
{
public:
    FrequencyAxis(const CommonParams& commonParams, AxesParams& axesParams, Buffers& buffers);

    virtual void UpdateValues();

protected:
    const CommonParams& m_commonParams;
    AxesParams&         m_axesParams;

    Buffers& m_buffers;
};

#endif // FREQUENCY_AXIS_HPP
