#include "DecimationAxis.hpp"
#include "AnalyzerParameters.hpp"

DecimationAxis::DecimationAxis(const CommonParams& commonParams, const AxesParams& axesParams, Buffers& buffers)
    : m_commonParams(commonParams),
      m_axesParams(axesParams),
      m_buffers(buffers)
{}

void DecimationAxis::UpdateValues()
{
    const int32_t samplesStep = m_commonParams.fftSize / m_axesParams.pointsToDisplay;
    int32_t samplesIdx = 0;
    for(int32_t idx = 0; idx < m_axesParams.pointsToDisplay; ++idx)
    {
        m_buffers.yValues[idx] = m_buffers.postDetectionOutput[samplesIdx];
        samplesIdx += samplesStep;
    }
}
