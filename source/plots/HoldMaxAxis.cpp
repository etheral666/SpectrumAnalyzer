#include "HoldMaxAxis.hpp"
#include "Types.hpp"
#include "AnalyzerParameters.hpp"

HoldMaxAxis::HoldMaxAxis(const CommonParams& commonParams, const AxesParams& axesParams, Buffers& buffers)
    : m_commonParams(commonParams),
      m_axesParams(axesParams),
      m_buffers(buffers)
{}

void HoldMaxAxis::UpdateValues()
{
    const int32_t samplesStep = m_commonParams.fftSize / m_axesParams.pointsToDisplay;
    int32_t samplesIdx = 0;
    for(int32_t idx = 0; idx < m_axesParams.pointsToDisplay; ++idx)
    {
        if(m_buffers.postDetectionOutput[samplesIdx] > m_buffers.yValues[idx])
        {
            m_buffers.yValues[idx] = m_buffers.postDetectionOutput[samplesIdx];
        }
        samplesIdx += samplesStep;
    }
}

