#include "FrequencyAxis.hpp"
#include "AnalyzerParameters.hpp"

FrequencyAxis::FrequencyAxis(const CommonParams& commonParams, AxesParams& axesParams, Buffers& buffers)
    : m_commonParams(commonParams),
      m_axesParams(axesParams),
      m_buffers(buffers)
{}

void FrequencyAxis::UpdateValues()
{
    const double scaler          = 1.f / m_axesParams.xAxisDivider;
    const double decimationRatio = static_cast<double>(m_commonParams.fftSize) / m_axesParams.pointsToDisplay;
    const double freqStep        = m_commonParams.samplingRate / m_commonParams.fftSize * scaler * decimationRatio;
    double       frequency       = m_commonParams.startFrequency * scaler;
    for(int32_t idx = 0; idx < m_axesParams.pointsToDisplay; ++idx)
    {
        m_buffers.xValues[idx] = frequency;
        frequency += freqStep;
    }
    m_axesParams.xMin = m_buffers.xValues[0];
    m_axesParams.xMax = m_buffers.xValues[m_axesParams.pointsToDisplay - 1];
}

