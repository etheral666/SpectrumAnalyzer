#ifndef HOLD_MAX_AXIS_HPP
#define HOLD_MAX_AXIS_HPP

#include "IAxis.hpp"

struct CommonParams;
struct AxesParams;
struct Buffers;

class HoldMaxAxis : public IAxis
{
public:
    HoldMaxAxis(const CommonParams& commonParams, const AxesParams& axesParams, Buffers& buffers);

    virtual void UpdateValues();

protected:
    const CommonParams& m_commonParams;
    const AxesParams&   m_axesParams;
    Buffers&            m_buffers;
};

#endif // HOLDMAXAXIS_HPP
