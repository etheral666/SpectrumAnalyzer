#ifndef DECIMATION_AXIS_HPP
#define DECIMATION_AXIS_HPP

#include "IAxis.hpp"

struct CommonParams;
struct AxesParams;
struct Buffers;

class DecimationAxis : public IAxis
{
public:
    DecimationAxis(const CommonParams& commonParams, const AxesParams& axesParams, Buffers& buffers);

    virtual void UpdateValues();

protected:
    const CommonParams& m_commonParams;
    const AxesParams&   m_axesParams;

    Buffers& m_buffers;
};

#endif // DECIMATION_AXIS_HPP
