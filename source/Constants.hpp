#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#include "Types.hpp"

struct Const
{
    static const int32_t fftSize1024  = 1024;
    static const int32_t fftSize2048  = 2048;
    static const int32_t fftSize4096  = 4096;
    static const int32_t fftSize8192  = 8192;
    static const int32_t fftSize16384 = 16384;

    static const int32_t samplingRate2MHz    = 2000000;
    static const int32_t samplingRate5MHz    = 5000000;
    static const int32_t samplingRate8MHz    = 8000000;
    static const int32_t samplingRate10MHz   = 10000000;
    static const int32_t samplingRate12d5MHz = 12500000;
    static const int32_t samplingRate16MHz   = 16000000;
    static const int32_t samplingRate20MHz   = 20000000;

    static const int32_t hackRfTransfersCount       = 4;
    static const int32_t hackRfTransferBufferSize   = 262144;
    static const int32_t hackRfTransferSamplesCount = hackRfTransferBufferSize / 2;

    static const int32_t scalerMega = 1000000;
    static const int32_t scalerKilo = 1000;
    static const int32_t noScaler   = 1;

    static const int32_t maxDspChainLinks = 3;

    static const int32_t plotRefreshPeriod = 40; //miliseconds
};

#endif // CONSTANTS_HPP

