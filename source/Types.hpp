#ifndef TYPES_HPP
#define TYPES_HPP

#include <stdint.h>
#include <complex>

#include <QVector>
#include <QPointF>

typedef std::complex<double> c64;
typedef std::complex<float>  c32;
typedef QVector<QPointF>::Iterator VecIterator;

#endif // TYPES_HPP

