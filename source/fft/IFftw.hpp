#ifndef IFFTW_HPP
#define IFFTW_HPP

#include "fftw.h"

class IFftw
{
public:
    virtual void*     FftwMalloc(size_t n) = 0;
    virtual void      FftwFree(void* p) = 0;
    virtual fftw_plan FftwCreatePlanSpecific(int            n,
                                             fftw_direction dir,
                                             int            flags,
                                             fftw_complex*  in,
                                             int            istride,
                                             fftw_complex*  out,
                                             int            ostride) = 0;
    virtual void      FftwExecute(fftw_plan     plan,
                                  int           howmany,
                                  fftw_complex* in,
                                  int           istride,
                                  int           idist,
                                  fftw_complex* out,
                                  int           ostride,
                                  int           odist) = 0;
    virtual void      FftwDestroyPlan(fftw_plan plan) = 0;
};

#endif // IFFTW_HPP

