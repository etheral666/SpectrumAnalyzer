#ifndef FFTW_HPP
#define FFTW_HPP

#include "fftw.h"

#include "IFftw.hpp"

class Fftw : public IFftw
{
public:
    virtual void* FftwMalloc(size_t n)
    {
        return fftw_malloc(n);
    }

    virtual void FftwFree(void* p)
    {
        fftw_free(p);
    }

    virtual void FftwExecute(fftw_plan     plan,
                             int           howmany,
                             fftw_complex* in,
                             int           istride,
                             int           idist,
                             fftw_complex* out,
                             int           ostride,
                             int           odist)
    {
        fftw(plan,
             howmany,
             in,
             istride,
             idist,
             out,
             ostride,
             odist);
    }

    virtual void FftwDestroyPlan(fftw_plan plan)
    {
        fftw_destroy_plan(plan);
    }

    virtual fftw_plan FftwCreatePlanSpecific(int            n,
                                             fftw_direction dir,
                                             int            flags,
                                             fftw_complex*  in,
                                             int            istride,
                                             fftw_complex*  out,
                                             int            ostride)
    {
        return fftw_create_plan_specific(n,
                                         dir,
                                         flags,
                                         in,
                                         istride,
                                         out,
                                         ostride);
    }
};

#endif // FFTW_HPP

