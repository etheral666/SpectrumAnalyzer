#ifndef FFTW_WRAPPER_HPP
#define FFTW_WRAPPER_HPP

#include "IFftw.hpp"
#include "Types.hpp"

struct CommonParams;
struct FftParams;
struct Buffers;

class FftwWrapper
{
public:
    FftwWrapper(const CommonParams& commonParams, FftParams& fftParams, Buffers& buffers);
    ~FftwWrapper();

    void ComputeFFT();
    void UpdateFftwParameters();

private:
    void AllocateResources();
    void FreeResources();

    IFftw*        m_fftw;

    const CommonParams& m_commonParams;

    FftParams& m_fftParams;
    Buffers&   m_buffers;
};

#endif // FFTW_WRAPPER_HPP
