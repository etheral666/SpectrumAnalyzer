#include "FftwWrapper.hpp"
#include "AnalyzerParameters.hpp"

#include <iostream>

FftwWrapper::FftwWrapper(const CommonParams& commonParams, FftParams& fftParams, Buffers& buffers)
    : m_fftw(fftParams.fftwEngine),
      m_commonParams(commonParams),
      m_fftParams(fftParams),
      m_buffers(buffers)

{
    AllocateResources();
}

FftwWrapper::~FftwWrapper()
{
    FreeResources();
}

void FftwWrapper::AllocateResources()
{
    std::cout << "[INFO] Allocating FFT buffers." << std::endl;

    const int32_t bufferSize = sizeof(c64) * m_commonParams.samplesCount;
    m_buffers.fftInput       = (c64*) m_fftw->FftwMalloc(bufferSize);
    m_buffers.fftOutput      = (c64*) m_fftw->FftwMalloc(bufferSize);

    std::cout << "[INFO] Creating FFTW plan: fftSize/fftSign/fftFlags = " << m_commonParams.fftSize << "/"
              << m_fftParams.fftSign << "/" << m_fftParams.fftFlags << std::endl;

    const int32_t inOutStride = 1;
    m_fftParams.fftwSpecificPlan = m_fftw->FftwCreatePlanSpecific(m_commonParams.fftSize,
                                                                  m_fftParams.fftSign,
                                                                  m_fftParams.fftFlags,
                                                                  reinterpret_cast<fftw_complex*>(m_buffers.fftInput),
                                                                  inOutStride,
                                                                  reinterpret_cast<fftw_complex*>(m_buffers.fftOutput),
                                                                  inOutStride);
}

void FftwWrapper::FreeResources()
{
    m_fftw->FftwDestroyPlan(m_fftParams.fftwSpecificPlan);
    m_fftw->FftwFree(m_buffers.fftInput);
    m_fftw->FftwFree(m_buffers.fftOutput);
}

void FftwWrapper::ComputeFFT()
{
    const int32_t inOutStride = 1;
    m_fftw->FftwExecute(m_fftParams.fftwSpecificPlan,
                        m_commonParams.numOfFfts,
                        reinterpret_cast<fftw_complex*>(m_buffers.fftInput),
                        inOutStride,
                        m_commonParams.fftSize,
                        reinterpret_cast<fftw_complex*>(m_buffers.fftOutput),
                        inOutStride,
                        m_commonParams.fftSize);
}

void FftwWrapper::UpdateFftwParameters()
{
    FreeResources();
    AllocateResources();
}

