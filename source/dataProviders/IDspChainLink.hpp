#ifndef IDSP_CHAIN_LINK
#define IDSP_CHAIN_LINK

class IDspChainLink
{
public:
    virtual void Execute() = 0;
};

#endif // IDSP_CHAIN_LINK

