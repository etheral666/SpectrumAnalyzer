#include "AbsCalculator.hpp"
#include "AnalyzerParameters.hpp"

#include <cmath>

AbsCalculator::AbsCalculator(const CommonParams& commonParams, Buffers& buffers)
    : m_commonParams(commonParams),
      m_buffers(buffers)
{}

void AbsCalculator::Execute()
{
    for(int32_t idx = 0; idx < m_commonParams.fftSize * m_commonParams.numOfFfts; ++idx)
    {
        m_buffers.postDetectionInput[idx] = std::abs(m_buffers.fftOutput[idx]);
    }
}

