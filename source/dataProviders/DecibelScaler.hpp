#ifndef DECIBEL_SCALER_HPP
#define DECIBEL_SCALER_HPP

#include "IDspChainLink.hpp"

struct CommonParams;
struct Buffers;

class DecibelScaler : public IDspChainLink
{
public:
    DecibelScaler(const CommonParams& commonParams, Buffers& buffers);

    virtual void Execute();

protected:
    const CommonParams& m_commonParams;

    Buffers& m_buffers;
};

#endif // DECIBEL_SCALER_HPP
