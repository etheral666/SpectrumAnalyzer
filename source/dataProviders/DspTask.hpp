#ifndef DSP_PROCESS_HPP
#define DSP_PROCESS_HPP

#include "Types.hpp"
#include "FftwWrapper.hpp"
#include "IDspChainLink.hpp"
#include "Constants.hpp"

class HackRfInterface;
struct CommonParams;
struct DspParams;
struct FftParams;
struct Buffers;

class DspTask
{
public:
    DspTask(CommonParams& commonParams,
            DspParams&    dspParams,
            FftParams&    fftParams,
            Buffers&      buffers);

    bool ProcessNextFrame();

private:
    CommonParams&    m_commonParams;
    DspParams&       m_dspParams;
    Buffers&         m_buffers;

    HackRfInterface* m_hackRf;
    FftwWrapper      m_fftw;
};

#endif // DSP_PROCESS_HPP
