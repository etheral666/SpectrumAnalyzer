#include "DspTask.hpp"
#include "HackRfInterface.hpp"
#include "SamplesAdapter.hpp"
#include "Globals.hpp"
#include "AnalyzerParameters.hpp"

#include <algorithm>

DspTask::DspTask(CommonParams& commonParams,
                 DspParams&    dspParams,
                 FftParams&    fftParams,
                 Buffers&      buffers)
    : m_commonParams(commonParams),
      m_dspParams(dspParams),
      m_buffers(buffers),
      m_fftw(commonParams, fftParams, buffers)
{
    std::fill_n(m_buffers.fftInput,  m_commonParams.fftSize * m_commonParams.numOfFfts, c64(0.f, 0.f));
    std::fill_n(m_buffers.fftOutput, m_commonParams.fftSize * m_commonParams.numOfFfts, c64(0.f, 0.f));

    m_hackRf = HackRfInterface::GetInstance();
    m_hackRf->StartReceiving();
}

bool DspTask::ProcessNextFrame()
{
    if(m_hackRf->WaitForNextFrame())
    {
        UInt8ToCplxDouble(Global::lastTransferInfo.buffer,
                          m_buffers.fftInput,
                          Global::lastTransferInfo.valid_length);
        m_fftw.ComputeFFT();
        for(uint32_t linkIdx = 0; linkIdx < m_dspParams.dspChain.size(); ++linkIdx)
        {
            m_dspParams.dspChain[linkIdx]->Execute();
        }
        return true;
    }
    else
    {
        return false;
    }
}

