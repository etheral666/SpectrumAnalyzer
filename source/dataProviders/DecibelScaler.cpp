#include "DecibelScaler.hpp"
#include "AnalyzerParameters.hpp"

#include <cmath>

DecibelScaler::DecibelScaler(const CommonParams& commonParams, Buffers& buffers)
    : m_commonParams(commonParams),
      m_buffers(buffers)
{}

void DecibelScaler::Execute()
{
    for(int32_t idx = 0; idx < m_commonParams.fftSize; ++idx)
    {
        m_buffers.postDetectionOutput[idx] = 10 * std::log10(m_buffers.postDetectionOutput[idx]);
    }
}

