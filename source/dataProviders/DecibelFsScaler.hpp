#ifndef DECIBEL_FS_SCALER_HPP
#define DECIBEL_FS_SCALER_HPP

#include "IDspChainLink.hpp"

struct CommonParams;
struct Buffers;

class DecibelFsScaler : public IDspChainLink
{
public:
    DecibelFsScaler(const CommonParams& commonParams, Buffers& buffers);

    virtual void Execute();

protected:
    const CommonParams& m_commonParams;

    Buffers& m_buffers;
};

#endif // DECIBEL_FS_SCALER_HPP
