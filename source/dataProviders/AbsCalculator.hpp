#ifndef ABS_CALCULATOR_HPP
#define ABS_CALCULATOR_HPP

#include "IDspChainLink.hpp"

struct CommonParams;
struct Buffers;

class AbsCalculator : public IDspChainLink
{
public:
    AbsCalculator(const CommonParams& commonParams, Buffers& buffers);

    virtual void Execute();

protected:
    const CommonParams& m_commonParams;

    Buffers& m_buffers;
};

#endif // ABS_CALCULATOR_HPP
