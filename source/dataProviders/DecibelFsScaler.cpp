#include "DecibelFsScaler.hpp"
#include "AnalyzerParameters.hpp"

#include <cmath>

DecibelFsScaler::DecibelFsScaler(const CommonParams& commonParams, Buffers& buffers)
    : m_commonParams(commonParams),
      m_buffers(buffers)
{}

void DecibelFsScaler::Execute()
{
    const double maxFftPeak          = std::abs(c64(127.f, 127.f)) * m_commonParams.fftSize;
    const double fullScaleCorrection = 10 * std::log10(maxFftPeak);
    for(int32_t idx = 0; idx < m_commonParams.fftSize; ++idx)
    {
        m_buffers.postDetectionOutput[idx] = 10 * std::log10(m_buffers.postDetectionOutput[idx]) - fullScaleCorrection;
    }
}

