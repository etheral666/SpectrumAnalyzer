#include "PostDetectionFilter.hpp"
#include "AnalyzerParameters.hpp"

#include <algorithm>

PostDetectionFilter::PostDetectionFilter(const CommonParams& commonParams, Buffers& buffers)
    : m_commonParams(commonParams),
      m_buffers(buffers)
{}

void PostDetectionFilter::Execute()
{
    std::fill_n(m_buffers.postDetectionOutput, m_commonParams.fftSize, 0.f);
    for(int32_t fftIdx = 0; fftIdx < m_commonParams.numOfFfts; ++fftIdx)
    {
        for(int32_t idx = 0; idx < m_commonParams.fftSize; ++idx)
        {
            m_buffers.postDetectionOutput[idx] += m_buffers.postDetectionInput[fftIdx * m_commonParams.fftSize + idx];
        }
    }

    for(int32_t idx = 0; idx < m_commonParams.fftSize; ++idx)
    {
        m_buffers.postDetectionOutput[idx] /= m_commonParams.numOfFfts;
    }
}

