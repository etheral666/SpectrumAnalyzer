#ifndef SAMPLES_ADAPTER_HPP
#define SAMPLES_ADAPTER_HPP

#include "Types.hpp"

void UInt8ToCplxFloat(uint8_t* input, c32* output, int32_t inputLength);

void UInt8ToCplxDouble(uint8_t* input, c64* output, int32_t inputLength);

#endif // SAMPLES_ADAPTER_HPP
