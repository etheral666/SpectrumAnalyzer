#ifndef POSTDETECTION_FILTER_HPP
#define POSTDETECTION_FILTER_HPP

#include "Types.hpp"
#include "IDspChainLink.hpp"

struct CommonParams;
struct Buffers;

class PostDetectionFilter : public IDspChainLink
{
public:
    PostDetectionFilter(const CommonParams& commonParams, Buffers& buffers);

    virtual void Execute();

protected:
    const CommonParams& m_commonParams;

    Buffers& m_buffers;
};

#endif // POSTDETECTION_FILTER_HPP
