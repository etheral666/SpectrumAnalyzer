#ifndef HACKRF_INTERFACE_HPP
#define HACKRF_INTERFACE_HPP

#include "Types.hpp"

#include <hackrf.h>

#include <string>

struct AnalyzerParameters;

class HackRfInterface
{
public:
    static HackRfInterface* GetInstance();

    ~HackRfInterface();

    bool WaitForNextFrame();

    bool StartReceiving();

    bool SetAnalysisParameters(const AnalyzerParameters& params);

    bool IsReady();

    const std::string& GetLastErrorDescription() const
    {
        return m_lastError;
    }

private:
    HackRfInterface();

    static HackRfInterface* s_instance;
    volatile static bool    s_isNextFrameReady;

    static int TransferCallback(hackrf_transfer* transfer);

    void SetErrorString(const std::string& description, const int32_t errCode);

    hackrf_device* m_device;

    std::string  m_lastError;
};

#endif // HACKRF_INTERFACE_HPP
