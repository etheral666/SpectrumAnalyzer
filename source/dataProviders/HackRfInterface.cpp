#include "HackRfInterface.hpp"
#include "AnalyzerParameters.hpp"
#include "Defaults.hpp"
#include "Globals.hpp"

#include <iostream>
#include <sstream>

HackRfInterface* HackRfInterface::s_instance         = NULL;
volatile bool    HackRfInterface::s_isNextFrameReady = false;

HackRfInterface::HackRfInterface()
    : m_device(NULL)
{
    int32_t status = hackrf_init();
    if(HACKRF_SUCCESS != hackrf_init())
    {
        SetErrorString("Initialization failed: ", status);
        return;
    }

    status = hackrf_open(&m_device);
    if(status != HACKRF_SUCCESS)
    {
        SetErrorString("Connection open failed: ", status);
        return;
    }

    status = hackrf_set_freq(m_device, Defaults::startFrequency);
    if(status != HACKRF_SUCCESS)
    {
        SetErrorString("Failed to set center frequency: ", status);
    }

    status = hackrf_set_sample_rate(m_device, Defaults::samplingRate);
    if(status != HACKRF_SUCCESS)
    {
        SetErrorString("Failed to set sample rate: ", status);
    }

    int32_t filter = hackrf_compute_baseband_filter_bw(Defaults::samplingRate);
    status = hackrf_set_baseband_filter_bandwidth(m_device, filter);
    if(status != HACKRF_SUCCESS)
    {
        SetErrorString("Failed to set baseband filter: ", status);
    }

    status = hackrf_set_vga_gain(m_device, Defaults::hackRfVgaGain);
    if(status != HACKRF_SUCCESS)
    {
        SetErrorString("Invalid VGA gain value; ", status);
    }

    status = hackrf_set_lna_gain(m_device, Defaults::hackRfIfGain);
    if(status != HACKRF_SUCCESS)
    {
        SetErrorString("Invalid IF gain value; ", status);
    }
}

HackRfInterface::~HackRfInterface()
{
    hackrf_close(m_device);
    hackrf_exit();
}

HackRfInterface* HackRfInterface::GetInstance()
{
    if(s_instance != NULL)
    {
        return s_instance;
    }
    else
    {
        s_instance = new HackRfInterface();
        if(s_instance->IsReady())
        {
            return s_instance;
        }
        else
        {
            s_instance->~HackRfInterface();
            return NULL;
        }
    }
}

bool HackRfInterface::SetAnalysisParameters(const AnalyzerParameters& params)
{
    int32_t result = hackrf_set_freq(m_device, params.common.startFrequency);

    if(result != HACKRF_SUCCESS)
    {
        SetErrorString("Failed to set start frequency; ", result);
        return false;
    }

    result = hackrf_set_sample_rate(m_device, static_cast<double>(params.common.samplingRate));
    if(result != HACKRF_SUCCESS)
    {
        SetErrorString("Failed to set sample rate; ", result);
        return false;
    }

    int32_t filter = hackrf_compute_baseband_filter_bw(params.common.samplingRate);
    result = hackrf_set_baseband_filter_bandwidth(m_device, filter);
    if(result != HACKRF_SUCCESS)
    {
        SetErrorString("Failed to set baseband filter; ", result);
        return false;
    }

    result = hackrf_set_vga_gain(m_device, params.hackRfParams.vgaGain);
    if(result != HACKRF_SUCCESS)
    {
        SetErrorString("Invalid VGA gain value; ", result);
        return false;
    }

    return true;
}

bool HackRfInterface::IsReady()
{
    return s_instance && m_device && hackrf_is_streaming(m_device);
}

int HackRfInterface::TransferCallback(hackrf_transfer* transfer)
{
    Global::lastTransferInfo = *transfer;
    s_isNextFrameReady       = true;
    return 0;
}

bool HackRfInterface::WaitForNextFrame()
{
    int32_t counter = 0;
    while(s_isNextFrameReady != true)
    {
        if(counter > 1000000)
        {
            s_instance->SetErrorString("HackRf unexpectedly stopped: ", hackrf_is_streaming(m_device));//HACKRF_ERROR_STREAMING_STOPPED);
            return false;
        }
        ++counter;
    }
    s_isNextFrameReady = false;
    return true;
}

bool HackRfInterface::StartReceiving()
{
    int result = hackrf_start_rx(m_device, TransferCallback, NULL);
    if(result < 0)
    {
        SetErrorString("Receiving could not be started: ", result);
        return false;
    }
    else
    {
        return true;
    }
}

void HackRfInterface::SetErrorString(const std::string& description, const int errCode)
{
    std::stringstream errorStream;
    errorStream << "[HackRf] Error: " << description << hackrf_error_name(static_cast<hackrf_error>(errCode));
    m_lastError = errorStream.str();
}
