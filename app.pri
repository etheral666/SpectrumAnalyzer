#qmake input for QT gui application targets

QT += gui core widgets

TEMPLATE = app

SOURCES += main/main.cpp \
           gui/MainWindow.cpp \
           gui/ChangeParamsForm.cpp \

HEADERS += gui/MainWindow.hpp \
           gui/ChangeParamsForm.hpp

FORMS   += gui/MainWindow.ui \
           gui/ChangeParamsForm.ui

