#qmake input for common targets

QT_VERSION = 5

QMAKE_CXXFLAGS += -std=c++0x

INCLUDEPATH += $$PWD/gui \
               $$PWD/main \
               $$PWD/source \
               $$PWD/source/plots \
               $$PWD/source/fft \
               $$PWD/source/dataProviders \
               $$PWD/tests \
               $$PWD/externals


HEADERS += $$PWD/source/fft/Fftw.hpp \
           $$PWD/source/fft/FftwWrapper.hpp \
           $$PWD/source/fft/IFftw.hpp \
           $$PWD/source/dataProviders/HackRfInterface.hpp \
           $$PWD/source/dataProviders/SamplesAdapter.hpp \
           $$PWD/source/Types.hpp \
           $$PWD/source/Constants.hpp \
           $$PWD/source/AnalyzerParameters.hpp \
           $$PWD/source/plots/IAxis.hpp \
           $$PWD/source/plots/FrequencyAxis.hpp \
           $$PWD/source/plots/DecimationAxis.hpp \
           $$PWD/source/plots/HoldMaxAxis.hpp \
           $$PWD/source/dataProviders/DspTask.hpp \
           $$PWD/source/dataProviders/IDspChainLink.hpp \
           $$PWD/source/dataProviders/PostDetectionFilter.hpp \
           $$PWD/source/dataProviders/AbsCalculator.hpp \
           $$PWD/source/dataProviders/DecibelScaler.hpp \
           $$PWD/source/dataProviders/DecibelFsScaler.hpp \

SOURCES += $$PWD/source/fft/FftwWrapper.cpp \
           $$PWD/source/dataProviders/HackRfInterface.cpp \
           $$PWD/source/dataProviders/SamplesAdapter.cpp \
           $$PWD/source/plots/FrequencyAxis.cpp \
           $$PWD/source/plots/DecimationAxis.cpp \
           $$PWD/source/plots/HoldMaxAxis.cpp \
           $$PWD/source/dataProviders/DspTask.cpp \
           $$PWD/source/Globals.cpp \
           $$PWD/source/dataProviders/PostDetectionFilter.cpp \
           $$PWD/source/dataProviders/AbsCalculator.cpp \
           $$PWD/source/dataProviders/DecibelScaler.cpp \
           $$PWD/source/dataProviders/DecibelFsScaler.cpp \

# external includes and libraries
QMAKE_RPATHDIR += $$PWD/externals/libhackrf/lib
QMAKE_RPATHDIR += $$PWD/externals/fftw-2.1.5/lib
QMAKE_RPATHDIR += $$PWD/externals/qwt-6.1.2/lib

INCLUDEPATH += $$PWD/externals/libhackrf/src
INCLUDEPATH += $$PWD/externals/fftw-2.1.5/include
INCLUDEPATH += $$PWD/externals/qwt-6.1.2/include

LIBS += -L$$PWD/externals/fftw-2.1.5/lib -lfftw
LIBS += -L$$PWD/externals/qwt-6.1.2/lib -lqwt
LIBS += -L$$PWD/externals/libhackrf/lib -lhackrf





