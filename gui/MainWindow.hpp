#ifndef MAIN_WINDOW_HPP
#define MAIN_WINDOW_HPP

#include "Types.hpp"
#include "FrequencyAxis.hpp"
#include "DecimationAxis.hpp"
#include "DspTask.hpp"
#include "Fftw.hpp"
#include "ChangeParamsForm.hpp"
#include "AnalyzerParameters.hpp"
#include "IDspChainLink.hpp"
#include "AbsCalculator.hpp"
#include "PostDetectionFilter.hpp"
#include "DecibelScaler.hpp"
#include "DecibelFsScaler.hpp"
#include "HoldMaxAxis.hpp"

#include "qwt_plot.h"
#include "qwt_plot_curve.h"
#include "qwt_plot_grid.h"

#include <QMainWindow>
#include <QTimer>
#include <QDialog>

#include <vector>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget* parent = 0);

    ~MainWindow();

private slots:
    void ProcessNextBlock();

    void on_m_changeParamsButton_clicked();

private:
    void InitializeAnalyzerParameters();

    void SetPlotAxesLabels();

    void SetAxesScales();

    Ui::MainWindow*  ui;
    ChangeParamsForm m_paramsForm;

    std::vector<double> m_postDetectionInput;
    std::vector<double> m_postDetectionOut;
    std::vector<double> m_yValues;
    std::vector<double> m_xValues;

    AbsCalculator       m_absCalculator;
    PostDetectionFilter m_postDetectionFilter;
    DecibelScaler       m_dbScaler;
    DecibelFsScaler     m_dbFsScaler;

    AnalyzerParameters m_parameters;

    Fftw m_fftEngine;

    DspTask* m_dspTask;

    QwtPlotCurve m_mainPlotCurve;
    QwtPlotGrid  m_plotGrid;

    FrequencyAxis  m_frequency;
    DecimationAxis m_decimationAxis;
    HoldMaxAxis    m_holdMaxAxis;

    QTimer  m_processTimer;
};

#endif // MAIN_WINDOW_HPP
