#include "ChangeParamsForm.hpp"
#include "ui_ChangeParamsForm.h"
#include "Constants.hpp"
#include "Types.hpp"
#include "AnalyzerParameters.hpp"

ChangeParamsForm::ChangeParamsForm(CommonParams& commonParams,
                                   HackRfParams& hackRfParams,
                                   DspParams&    dspParams,
                                   AxesParams&   axesParams,
                                   QWidget*      parent)
    : QDialog(parent),
      ui(new Ui::ChangeParamsForm),
      m_commonParams(commonParams),
      m_hackRfParams(hackRfParams),
      m_dspParams(dspParams),
      m_axesParams(axesParams),
      m_dbFsScaleActive(true),
      m_holdMaxAcitve(false)
{
    ui->setupUi(this);
    this->setSizeGripEnabled(false);
    this->setModal(true);
    LoadValuesToForm();
}

ChangeParamsForm::~ChangeParamsForm()
{
    delete ui;
}

void ChangeParamsForm::LoadValuesToForm()
{
    const double centerFrequency = m_commonParams.startFrequency + m_commonParams.bandwidth;
    ui->m_centerFrequencySpinBox->setValue(centerFrequency / Const::scalerMega);
    ui->m_vgaGainSpinBox->setValue(m_hackRfParams.vgaGain);
}

const QString& ChangeParamsForm::GetAmplitudeAxisLabel()
{
    return m_xAxisLabel;
}

void ChangeParamsForm::on_m_acceptButton_clicked()
{
    // TODO validate params

    m_hackRfParams.vgaGain = ui->m_vgaGainSpinBox->value();

    SetSamplingRate();
    m_commonParams.bandwidth = m_commonParams.samplingRate / 2;

    const double centerFrequency = ui->m_centerFrequencySpinBox->value() * Const::scalerMega;
    m_commonParams.startFrequency = centerFrequency - m_commonParams.bandwidth;

    SetDspChain();

    if(ui->m_holdMaxCheckBox->isChecked())
    {
        m_axesParams.yAxisCalculator = m_axesParams.holdMaxAxis;
    }
    else
    {
        m_axesParams.yAxisCalculator = m_axesParams.decimationAxis;
    }

    this->done(QDialog::Accepted);
}

void ChangeParamsForm::SetDspChain()
{
    m_dspParams.dspChain.clear();
    m_dspParams.dspChain.push_back(m_dspParams.absCalculator);
    m_dspParams.dspChain.push_back(m_dspParams.postDetector);
    if(m_dbFsScaleActive)
    {
        m_dspParams.dspChain.push_back(m_dspParams.dbFsScaler);
        m_axesParams.yMin = -45.f;
        m_axesParams.yMax = 0.f;
        m_xAxisLabel = "Amplitude [dbFS]";
    }
    else
    {
        m_dspParams.dspChain.push_back(m_dspParams.dbScaler);
        m_axesParams.yMin = 0.f;
        m_axesParams.yMax = 100.f;
        m_xAxisLabel = "Amplitude [db]";
    }
}

void ChangeParamsForm::SetSamplingRate()
{
    switch(ui->m_bandwidthComboBox->currentIndex())
    {
        case RATE2MHZ:
        {
            m_commonParams.samplingRate = Const::samplingRate2MHz;
            break;
        }
        case RATE5MHZ:
        {
            m_commonParams.samplingRate = Const::samplingRate5MHz;
            break;
        }
        case RATE8MHZ:
        {
            m_commonParams.samplingRate = Const::samplingRate8MHz;
            break;
        }
        case RATE10MHZ:
        {
            m_commonParams.samplingRate = Const::samplingRate10MHz;
            break;
        }
        case RATE12d5MHZ:
        {
            m_commonParams.samplingRate = Const::samplingRate12d5MHz;
            break;
        }
        case RATE16MHZ:
        {
            m_commonParams.samplingRate = Const::samplingRate16MHz;
            break;
        }
        case RATE20MHZ:
        {
            m_commonParams.samplingRate = Const::samplingRate20MHz;
            break;
        }
    }
}

void ChangeParamsForm::on_m_cancelButton_clicked()
{
    this->done(QDialog::Rejected);
}

void ChangeParamsForm::on_m_dbFsPlotRadioButton_toggled(bool checked)
{
    if(checked)
    {
        ui->m_dbPlotRadioButton->setChecked(false);
        m_dbFsScaleActive = true;
    }
    else
    {
        m_dbFsScaleActive = false;
    }
}

void ChangeParamsForm::on_m_dbPlotRadioButton_toggled(bool checked)
{
    if(checked)
    {
        ui->m_dbFsPlotRadioButton->setChecked(false);
        m_dbFsScaleActive = false;
    }
    else
    {
        m_dbFsScaleActive = true;
    }
}
