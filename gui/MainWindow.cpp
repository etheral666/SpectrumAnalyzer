#include "MainWindow.hpp"
#include "ui_MainWindow.h"
#include "Constants.hpp"
#include "Defaults.hpp"
#include "SamplesAdapter.hpp"
#include "IAxis.hpp"
#include "Globals.hpp"
#include "IFftw.hpp"
#include "HackRfInterface.hpp"

#include "hackrf.h"

#include <Qt>
#include <QThread>
#include <QCoreApplication>

#include <cstring>
#include <iostream>
#include <algorithm>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow),
      m_paramsForm(m_parameters.common,
                   m_parameters.hackRfParams,
                   m_parameters.dsp,
                   m_parameters.axes,
                   parent),
      m_absCalculator(m_parameters.common, m_parameters.buffers),
      m_postDetectionFilter(m_parameters.common, m_parameters.buffers),
      m_dbScaler(m_parameters.common, m_parameters.buffers),
      m_dbFsScaler(m_parameters.common, m_parameters.buffers),
      m_frequency(m_parameters.common, m_parameters.axes, m_parameters.buffers),
      m_decimationAxis(m_parameters.common, m_parameters.axes, m_parameters.buffers),
      m_holdMaxAxis(m_parameters.common, m_parameters.axes, m_parameters.buffers)
{
    ui->setupUi(this);

    InitializeAnalyzerParameters();

    m_dspTask = new DspTask(m_parameters.common,
                            m_parameters.dsp,
                            m_parameters.fft,
                            m_parameters.buffers);

    m_frequency.UpdateValues();

    m_mainPlotCurve.attach(ui->m_qwtPlot);
    m_mainPlotCurve.setRawSamples(&m_xValues[0], &m_yValues[0], m_parameters.axes.pointsToDisplay);
    SetPlotAxesLabels();
    SetAxesScales();
    ui->m_qwtPlot->updateAxes();

    m_plotGrid.attach(ui->m_qwtPlot);
    m_plotGrid.enableXMin(true);
    m_plotGrid.enableYMin(true);
    m_plotGrid.setPen(QColor("gray"), 0.f, Qt::DashLine);

    ui->m_qwtPlot->replot();

    QThread::connect(&m_processTimer, &QTimer::timeout, this, &MainWindow::ProcessNextBlock);
    m_processTimer.start(Const::plotRefreshPeriod);
}

MainWindow::~MainWindow()
{
    m_processTimer.stop();
    delete m_dspTask;
    delete ui;
}

void MainWindow::InitializeAnalyzerParameters()
{
    std::memset(&m_parameters, 0, sizeof(AnalyzerParameters));

    CommonParams& common = m_parameters.common;
    common.startFrequency = Defaults::startFrequency;
    common.samplingRate   = Defaults::samplingRate;
    common.bandwidth      = Defaults::bandwidth;
    common.fftSize        = Const::fftSize2048;
    common.samplesCount   = Const::hackRfTransferSamplesCount;
    common.numOfFfts      = common.samplesCount / common.fftSize;

    AxesParams& axes = m_parameters.axes;
    axes.decimationAxis  = &m_decimationAxis;
    axes.holdMaxAxis     = &m_holdMaxAxis;
    axes.yAxisCalculator = &m_decimationAxis;
    axes.pointsToDisplay = Defaults::pointsOnPlot;
    axes.xAxisDivider    = Const::scalerMega;
    axes.yMin            = -45.f;
    axes.yMax            = 0.f;

    FftParams& fft = m_parameters.fft;
    fft.fftwEngine = &m_fftEngine;
    fft.fftFlags   = FFTW_MEASURE;
    fft.fftSign    = FFTW_FORWARD;

    m_postDetectionInput.assign(common.fftSize * common.numOfFfts, 0.f);
    m_postDetectionOut.assign(common.fftSize, 0.f);
    m_xValues.assign(axes.pointsToDisplay, 0.f);
    m_yValues.assign(axes.pointsToDisplay, 0.f);

    Buffers& buffers = m_parameters.buffers;
    buffers.postDetectionOutput = &m_postDetectionOut[0];
    buffers.postDetectionInput  = &m_postDetectionInput[0];
    buffers.xValues             = &m_xValues[0];
    buffers.yValues             = &m_yValues[0];

    DspParams& dsp = m_parameters.dsp;
    dsp.dspChain.clear();
    dsp.dspChain.push_back(&m_absCalculator);
    dsp.dspChain.push_back(&m_postDetectionFilter);
    dsp.dspChain.push_back(&m_dbFsScaler);
    dsp.absCalculator = &m_absCalculator;
    dsp.dbScaler      = &m_dbScaler;
    dsp.dbFsScaler    = &m_dbFsScaler;
    dsp.postDetector  = &m_postDetectionFilter;

    HackRfParams& hackRf = m_parameters.hackRfParams;
    hackRf.vgaGain = Defaults::hackRfVgaGain;

}

void MainWindow::SetPlotAxesLabels()
{
    ui->m_qwtPlot->setAxisTitle(QwtPlot::Axis::yLeft, "Amplitude [dBFS]");
    ui->m_qwtPlot->setAxisLabelAlignment(QwtPlot::Axis::yLeft, Qt::AlignCenter);
    ui->m_qwtPlot->setAxisTitle(QwtPlot::Axis::xBottom, "f [MHz]");
    ui->m_qwtPlot->setAxisLabelAlignment(QwtPlot::Axis::xBottom, Qt::AlignCenter);
}

void MainWindow::SetAxesScales()
{
    ui->m_qwtPlot->setAxisScale(QwtPlot::Axis::yLeft, m_parameters.axes.yMin, m_parameters.axes.yMax);
    ui->m_qwtPlot->setAxisScale(QwtPlot::Axis::xBottom, m_parameters.axes.xMin, m_parameters.axes.xMax);
}

void MainWindow::ProcessNextBlock()
{
    if(m_dspTask->ProcessNextFrame())
    {
        m_parameters.axes.yAxisCalculator->UpdateValues();
        m_mainPlotCurve.setRawSamples(&m_xValues[0], &m_yValues[0], m_parameters.axes.pointsToDisplay);
        ui->m_qwtPlot->replot();
    }
    else
    {
        std::cout << "[ERROR] " << HackRfInterface::GetInstance()->GetLastErrorDescription() << std::endl;
        std::cout << "[ERROR] HackRf not connected, exiting." << std::endl;
        QCoreApplication::exit(-1);
    }
}

void MainWindow::on_m_changeParamsButton_clicked()
{
    m_processTimer.stop();
    m_paramsForm.LoadValuesToForm();
    m_paramsForm.exec();
    if(QDialog::Accepted == m_paramsForm.result())
    {
        std::cout << "[INFO] Changed params: startFrequency/samplingRate (MHz): " << m_parameters.common.startFrequency / Const::scalerMega <<
                     " / " << m_parameters.common.samplingRate / Const::scalerMega << std::endl;
        m_frequency.UpdateValues();
        std::fill(m_yValues.begin(), m_yValues.end(), -100.f);
        SetAxesScales();
        ui->m_qwtPlot->setAxisTitle(QwtPlot::Axis::yLeft, m_paramsForm.GetAmplitudeAxisLabel());

        bool result = HackRfInterface::GetInstance()->SetAnalysisParameters(m_parameters);
        if(!result)
        {
            std::cout << "[ERROR] " << HackRfInterface::GetInstance()->GetLastErrorDescription() << std::endl;
        }
    }
    m_processTimer.start(Const::plotRefreshPeriod);
}
