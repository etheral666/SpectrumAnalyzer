#ifndef CHANGE_PARAMS_FORM_HPP
#define CHANGE_PARAMS_FORM_HPP

#include <QDialog>
#include <QString>

namespace Ui
{
class ChangeParamsForm;
}

struct CommonParams;
struct HackRfParams;
struct DspParams;
struct AxesParams;

class ChangeParamsForm : public QDialog
{
    Q_OBJECT

    enum SAMPLING_RATE
    {
        RATE2MHZ    = 0,
        RATE5MHZ    = 1,
        RATE8MHZ    = 2,
        RATE10MHZ   = 3,
        RATE12d5MHZ = 4,
        RATE16MHZ   = 5,
        RATE20MHZ   = 6
    };

public:
    explicit ChangeParamsForm(CommonParams& commonParams,
                              HackRfParams& hackRfParams,
                              DspParams&    dspParams,
                              AxesParams&   axesParams,
                              QWidget*      parent = 0);
    ~ChangeParamsForm();

    void LoadValuesToForm();

    const QString& GetAmplitudeAxisLabel();

private slots:
    void on_m_acceptButton_clicked();

    void on_m_cancelButton_clicked();

    void on_m_dbFsPlotRadioButton_toggled(bool checked);

    void on_m_dbPlotRadioButton_toggled(bool checked);

private:
    void SetDspChain();
    void SetSamplingRate();

    Ui::ChangeParamsForm *ui;

    CommonParams& m_commonParams;
    HackRfParams& m_hackRfParams;
    DspParams&    m_dspParams;
    AxesParams&   m_axesParams;

    bool m_dbFsScaleActive;
    bool m_holdMaxAcitve;

    QString m_xAxisLabel;
};

#endif // CHANGE_PARAMS_FORM_HPP
