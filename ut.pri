#qmake input for unit tests

INCLUDEPATH += tests \

SOURCES += $$PWD/main/mainUt.cpp \
           $$PWD/tests/UnitTests/FftwWrapperTests.cpp \
           $$PWD/tests/UnitTests/HackRfInterfaceTests.cpp \
           $$PWD/tests/UnitTests/SamplesAdapterTests.cpp \
           $$PWD/tests/UnitTests/FrequencyAxisTests.cpp \
           $$PWD/tests/UnitTests/HoldMaxAxisTests.cpp \
           $$PWD/tests/UnitTests/PostDetectionFilterTests.cpp \
           $$PWD/tests/UnitTests/AbsCalculatorTests.cpp \
           $$PWD/tests/UnitTests/DecibelScalerTests.cpp \
           $$PWD/tests/UnitTests/FftwTests.cpp \

HEADERS += $$PWD/tests/UnitTests/FftwWrapperMock.hpp \

#google test's libraries
INCLUDEPATH += $$PWD/externals/gmock-1.6.0/gtest/include
DEPENDPATH += $$PWD/externals/gmock-1.6.0/gtest/include

LIBS += -L$$PWD/externals/gmock-1.6.0/gmock_build -lgmock

INCLUDEPATH += $$PWD/externals/gmock-1.6.0/include
DEPENDPATH += $$PWD/externals/gmock-1.6.0/include

PRE_TARGETDEPS += $$PWD/externals/gmock-1.6.0/gmock_build/libgmock.a

